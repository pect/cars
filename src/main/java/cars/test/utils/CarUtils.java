package cars.test.utils;

import cars.test.dto.CarDto;
import cars.test.entities.Car;

public class CarUtils {
	
	public static Car carDtoToCar(CarDto carDto) {
		Car car = new Car();
		car.setId(carDto.getId());
		car.setModel(carDto.getModel());
		car.setColor(carDto.getColor());
		car.setBrand("N/A");
		car.setYear("N/A");
		return car;
	}
}
