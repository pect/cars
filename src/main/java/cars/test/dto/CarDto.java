package cars.test.dto;

public class CarDto {
	private long id;
	private String model;
	private String color;
	
	public CarDto() {}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public static class CarDtoBuilder {
		private long id;
		private String model;
		private String color;
		public CarDtoBuilder() {}
		public CarDtoBuilder id(long id) {
			this.id = id;
			return this;
		}
		public CarDtoBuilder model(String model) {
			this.model = model;
			return this;
		}
		public CarDtoBuilder color(String color) {
			this.color = color;
			return this;
		}
		public CarDto build() {
			CarDto car = new CarDto();
			car.setId(this.id);
			car.setModel(this.model);
			car.setColor(this.color);
			return car;
		}
	}
}
