package cars.test.dao;

import java.util.ArrayList;
import java.util.List;

import cars.test.entities.Car;

public class CarDaoDummyImpl implements CarDao{
	private static final List<Car> cars = new ArrayList<Car>();
	
	public CarDaoDummyImpl () {
		
	}

	@Override
	public List<Car> getCars() {
		if (cars.isEmpty()) {
			Car car1 = new Car();
			car1.setId(1);
			car1.setBrand("Audi");
			car1.setModel("A4");
			car1.setColor("Black");
			car1.setYear("2019");
			Car car2 = new Car();
			car2.setId(2);
			car2.setBrand("Bentley");
			car2.setModel("Continental GT");
			car2.setColor("Red");
			car2.setYear("2018");
			Car car3 = new Car();
			car3.setId(3);
			car3.setBrand("BMW");
			car3.setModel("X2");
			car3.setColor("Green");
			car3.setYear("2014");
			Car car4 = new Car();
			car4.setId(4);
			car4.setBrand("Borgward");
			car4.setModel("BX7");
			car4.setColor("White");
			car4.setYear("2020");
			System.out.println();
			cars.add(car1);
			cars.add(car2);
			cars.add(car3);
			cars.add(car4);
			System.out.println(cars);
		}
		return cars;
	}

	@Override
	public void update(Car car) {
		cars.add(car);
	}

}
