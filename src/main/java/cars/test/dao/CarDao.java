package cars.test.dao;

import java.util.List;

import cars.test.entities.Car;

public interface CarDao {
	
	List<Car> getCars();
	
	void update (Car car);
	
	static CarDao getInstance() {
		return new CarDaoDummyImpl();
	}
	
}
