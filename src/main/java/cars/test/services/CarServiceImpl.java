package cars.test.services;

import java.util.List;
import java.util.stream.Collectors;

import cars.test.dao.CarDao;
import cars.test.dto.CarDto;
import cars.test.entities.Car;
import cars.test.utils.CarUtils;

public class CarServiceImpl implements CarService {

	@Override
	public List<CarDto> getCars() {
		List<CarDto> listDto = null;
		final List<Car> list = CarDao.getInstance().getCars();
		listDto = list.stream()
				.map(car -> new CarDto.CarDtoBuilder()
						.id(car.getId())
						.model(car.getModel())
						.color(car.getColor())
						.build())
				.collect(Collectors.toList());
		return listDto;
	}

	@Override
	public void update(CarDto carDto) {
		CarDao.getInstance().update(CarUtils.carDtoToCar(carDto));
	}

}
