package cars.test.services;

import java.util.List;

import cars.test.dto.CarDto;
import cars.test.entities.Car;

public interface CarService {
	
	List<CarDto> getCars();
	
	void update(CarDto car);

	static CarService getInstance() {
		return new CarServiceImpl();
	}
}
