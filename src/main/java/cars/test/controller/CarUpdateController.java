package cars.test.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cars.test.dto.CarDto;
import cars.test.services.CarService;

@WebServlet("/update")
public class CarUpdateController extends HttpServlet {
	private static final long serialVersionUID = 7112486796709312267L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CarDto car = new CarDto.CarDtoBuilder()
				.id(Long.parseLong(request.getParameter("id")))
				.model(request.getParameter("model"))
				.color(request.getParameter("color"))
				.build();
		
		CarService.getInstance().update(car);
		request.getRequestDispatcher("/").forward(request, response);
	}
	
}
