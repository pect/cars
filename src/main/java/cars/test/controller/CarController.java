package cars.test.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cars.test.dto.CarDto;
import cars.test.services.CarService;

@WebServlet("/list")
public class CarController extends HttpServlet {
	private static final long serialVersionUID = 2864321800324547734L;
	
	@Override
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {
	}
	
	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {
		List<CarDto> cars = CarService.getInstance().getCars();
		request.setAttribute("cars", cars);
		request.getRequestDispatcher("/info/index.jsp").forward(request, response);
	}

}
