<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add Cars</title>
</head>
<body>
	<form name="createCar" action="/cars/update" method="post">
		<label>ID: </label><input name="id" type="number"><br>
		<label>Model: </label><input name="model" type="text"><br>
		<label>Color: </label><input name="color" type="text"><br>
		<input type="submit" value="Add Car"/>
	</form>
	<p><a href="/cars/index.jsp">Regresar</a></p>
</body>
</html>