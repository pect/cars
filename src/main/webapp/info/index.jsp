<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lista de coches</title>
</head>
<body>
	<h2>Car list</h2>
	<table>
		<tr>
			<th>ID</th>
			<th>Model</th>
			<th>Color</th>
		</tr>
		<c:forEach var="car" items="${cars}">
		<tr>
			<td>${car.id}</td>
			<td>${car.model}</td>
			<td>${car.color}</td>
		</tr>
		</c:forEach>
	</table>
	<p><a href="/cars/index.jsp">Regresar</a></p>
</body>
</html>